package connect

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"time"

	"gitlab.com/streamy/anno"
)

var (
	appID           = "streamy-connect"
	udpAnnouncePort = 6161
)

type bcTCPAnnounceType struct{}

func (at bcTCPAnnounceType) Announce(addr *Addr, netAddr net.Addr) (announcer, error) {

	var port int
	var ip net.IP
	switch a := netAddr.(type) {
	case *net.TCPAddr:
		ip = a.IP
		port = a.Port
	default:
		return nil, fmt.Errorf("Can't announce connection of type %T", a)
	}

	log.Printf("announcing %v on tcp via udp broadcast as ip %v\n", addr, ip)

	ctx, cancel := context.WithCancel(context.Background())

	errs := make(chan error, 1)

	annoData := annoData{addr.String(), port, "tcp"}
	data, err := json.Marshal(annoData)
	if err != nil {
		panic("this is a bug")
	}

	go func() {
		errs <- anno.Announce(ctx, anno.AnnounceConf{appID, udpAnnouncePort, data, 5 * time.Second})

	}()

	return &bcTCPAnnouncer{cancel, errs}, nil

}

type annoData struct {
	NodeID string
	Port   int
	Type   string // "utp", "tcp"
}

func (at bcTCPAnnounceType) CanAnnounce(netAddr net.Addr) bool {
	switch netAddr.(type) {
	case *net.TCPAddr:
		return true
	default:
		return false
	}
}

type bcTCPAnnouncer struct {
	cancel context.CancelFunc
	errors chan error
}

func (a *bcTCPAnnouncer) Close() error {
	a.cancel()
	err, ok := <-a.errors
	if ok {
		close(a.errors)
	}
	return err
}
