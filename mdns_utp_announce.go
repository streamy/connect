package connect

import (
	"fmt"
	"log"
	"net"

	"github.com/micro/mdns"
)

type mdnsUTPAnnounceType struct{}

func (at mdnsUTPAnnounceType) Announce(addr *Addr, netAddr net.Addr) (announcer, error) {
	serviceName, err := mdnsServiceName(addr.pub, "utp")
	if err != nil {
		return nil, err
	}

	var port int
	var ip net.IP
	switch a := netAddr.(type) {
	case *net.UDPAddr:
		ip = a.IP
		port = a.Port
	default:
		return nil, fmt.Errorf("Can't announce connection of type %T", a)
	}

	log.Printf("announcing %v on utp via mdns as ip %v\n", addr, ip)

	service, err := mdns.NewMDNSService(
		serviceName,
		serviceName,
		"",
		"",
		port,
		[]net.IP{ip},
		nil,
	)
	if err != nil {
		return nil, err
	}

	s, err := mdns.NewServer(
		&mdns.Config{
			Zone: service,
		},
	)
	if err != nil {
		return nil, err
	}

	return &mdnsUTPAnnouncer{s}, nil

}

func (at mdnsUTPAnnounceType) CanAnnounce(netAddr net.Addr) bool {
	switch netAddr.(type) {
	case *net.UDPAddr:
		return true
	default:
		return false
	}
}

type mdnsUTPAnnouncer struct {
	mdnsServer *mdns.Server
}

func (a *mdnsUTPAnnouncer) Close() error {
	return a.mdnsServer.Shutdown()

}
