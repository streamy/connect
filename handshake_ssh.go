package connect

import (
	"crypto"
	"crypto/subtle"
	"fmt"
	"golang.org/x/crypto/ssh"
	"io"
	"net"
	"sync"
)

type sshHandshaker struct {
	priv crypto.Signer
}

func newSSHHandshaker(signer crypto.Signer) (*sshHandshaker, error) {
	return &sshHandshaker{signer}, nil
}

func (hs *sshHandshaker) handshakeClient(conn Conn, serverKey crypto.PublicKey) (Conn, error) {
	clientSSHSigner, err := ssh.NewSignerFromKey(hs.priv)
	if err != nil {
		return nil, err
	}

	serverSSHPubKey, err := ssh.NewPublicKey(serverKey)
	if err != nil {
		return nil, err
	}

	conf := &ssh.ClientConfig{
		ClientVersion: versionString,
		HostKeyCallback: func(hostname string, remote net.Addr, k ssh.PublicKey) error {
			return sshCheckKeysEqual(k, serverSSHPubKey)
		},
		User: userName,
	}

	conf.Auth = []ssh.AuthMethod{ssh.PublicKeys(clientSSHSigner)}

	c, chans, reqs, err := ssh.NewClientConn(wrapConn(conn), "", conf)
	if err != nil {
		return nil, err
	}

	con := &sshReadWriteCloser{
		sshConn:    c,
		localAddr:  &Addr{hs.priv.Public()},
		remoteAddr: &Addr{serverKey},
	}

	con.discardAllReqs(reqs)

	ch, reqCh, err := c.OpenChannel(channelName, []byte{})
	if err != nil {
		_ = c.Close()
		return nil, err
	}

	con.streamConn = ch

	con.discardAllReqs(reqCh)

	con.exitWg.Add(1)
	go func() {
		defer con.exitWg.Done()
		for ch := range chans {
			_ = ch.Reject(ssh.UnknownChannelType, fmt.Sprintf("unknown channel type: %v", ch.ChannelType()))
		}
	}()
	return con, nil
}

func (hs *sshHandshaker) handshakeServer(conn Conn) (Conn, error) {
	serverSSHSigner, err := ssh.NewSignerFromKey(hs.priv)
	if err != nil {
		return nil, err
	}

	var authenticatedPK crypto.PublicKey

	conf := &ssh.ServerConfig{
		NoClientAuth: false,
		PublicKeyCallback: func(md ssh.ConnMetadata, k ssh.PublicKey) (*ssh.Permissions, error) {
			cpc, ok := k.(ssh.CryptoPublicKey)
			if !ok {
				return nil, fmt.Errorf("authentication failed due to unsupported key type %T", k)
			}
			authenticatedPK = cpc.CryptoPublicKey()
			return nil, nil
		},
		/*
			AuthLogCallback: func(cmd ssh.ConnMetadata, method string, err error) {
				var message string
				if err != nil {
					message = fmt.Sprintf("failed with error %s", err.Error())
				} else {
					message = "succeeded"
				}
				log.Printf("login by user %v from %v with version %s using %v %s", cmd.User(), cmd.RemoteAddr(), cmd.ClientVersion(), method, message)
			},
		*/
		ServerVersion: versionString,
	}
	conf.AddHostKey(serverSSHSigner)

	serverConn, newChan, Req, err := ssh.NewServerConn(wrapConn(conn), conf)
	if err != nil {
		return nil, err
	}

	rwc := &sshReadWriteCloser{
		sshConn:    serverConn,
		localAddr:  &Addr{hs.priv.Public()},
		remoteAddr: &Addr{authenticatedPK},
	}
	rwc.discardAllReqs(Req)
	streamConnCh := make(chan io.ReadWriteCloser)

	rwc.exitWg.Add(1)
	go func() {
		defer rwc.exitWg.Done()
		var gotStream bool
		for ch := range newChan {
			if ch.ChannelType() == channelName {
				sch, reqCh, err := ch.Accept()
				if err != nil {
					panic(err) //TODO: handle properly
				}
				if !gotStream {
					streamConnCh <- sch
					rwc.discardAllReqs(reqCh)
					gotStream = true
				} else {
					_ = ch.Reject(ssh.UnknownChannelType,
						fmt.Sprintf("unexpected request for channel '%v'. This might be a bug or an attack. Ignoring.", channelName),
					)
				}
			} else {
				_ = ch.Reject(ssh.UnknownChannelType, fmt.Sprintf("unknown channel type: %v", ch.ChannelType()))
			}
		}
	}()

	rwc.streamConn = <-streamConnCh

	return rwc, nil
}

const (
	channelName   = "streamy"
	userName      = "streamy"
	versionString = "SSH-2.0-streamy"
)

type sshReadWriteCloser struct {
	sshConn    ssh.Conn
	streamConn io.ReadWriteCloser
	exitWg     sync.WaitGroup
	localAddr  *Addr
	remoteAddr *Addr

	closeLk sync.Mutex
	closed  bool
}

func (s *sshReadWriteCloser) Read(buf []byte) (int, error) {
	return s.streamConn.Read(buf)
}

func (s *sshReadWriteCloser) Write(buf []byte) (int, error) {
	i, err := s.streamConn.Write(buf)
	if err == io.EOF {
		err = errHsConnectionClosed
	}
	return i, err
}

func (s *sshReadWriteCloser) Close() error {
	s.closeLk.Lock()
	defer func() {
		s.closed = true
		s.closeLk.Unlock()
	}()
	if s.closed {
		return errHsConnectionClosed
	}

	e := s.streamConn.Close()
	e2 := s.sshConn.Close()
	s.exitWg.Wait()
	if e == nil {
		e = e2
	}
	if e == io.EOF {
		e = nil
	}
	return e
}

func (s *sshReadWriteCloser) RemoteAddr() net.Addr {
	return s.remoteAddr
}

func (s *sshReadWriteCloser) LocalAddr() net.Addr {
	return s.localAddr
}

func (s *sshReadWriteCloser) discardAllReqs(reqs <-chan *ssh.Request) {
	s.exitWg.Add(1)
	go func() {
		defer s.exitWg.Done()
		for req := range reqs {
			_ = req.Reply(false, nil)
		}
	}()
}

func sshCheckKeysEqual(k1, k2 ssh.PublicKey) error {
	if k1.Type() != k2.Type() {
		return fmt.Errorf("mismatched key key types %v and %v", k1.Type(), k2.Type())
	}

	// TODO: does this need to be constant time? Is this really a risk here?
	if subtle.ConstantTimeCompare(k1.Marshal(), k2.Marshal()) != 1 {
		// TODO: is there a risk in logging this?
		return fmt.Errorf("Key mismatch :\n%x\n%x", k1.Marshal(), k2.Marshal())
	}
	return nil
}
