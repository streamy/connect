package connect

import (
	"fmt"
	"net"
)

type multiTransportDialler struct {
	tds []transportDialler
}

func newTransportDiallerMulti(transports ...transportDialler) transportDialler {
	return &multiTransportDialler{transports}
}

func (t *multiTransportDialler) Dial(addr net.Addr) (Conn, error) {
	diallers := t.filteredDiallers(addr)
	if len(diallers) == 0 {
		return nil, fmt.Errorf("can't dial addresses of type %T", addr)
	}
	var errs []error
	for _, td := range diallers {
		con, err := td.Dial(addr)
		if err == nil {
			return con, nil
		}
		errs = append(errs, err)
	}
	switch len(errs) {
	case 0:
		panic("this is a bug")
	case 1:
		return nil, errs[0]
	default:
		return nil, multiError{"Dialling failed", errs}
	}
}

/////

func (t *multiTransportDialler) CanDial(addr net.Addr) bool {
	return len(t.filteredDiallers(addr)) > 0
}

func (t *multiTransportDialler) filteredDiallers(addr net.Addr) []transportDialler {
	var filtered []transportDialler
	for _, td := range t.tds {
		if td.CanDial(addr) {
			filtered = append(filtered, td)
		}
	}
	return filtered
}
