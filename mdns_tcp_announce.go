package connect

import (
	"fmt"
	"log"
	"net"

	"github.com/micro/mdns"
)

type mdnsTCPAnnounceType struct{}

func (at mdnsTCPAnnounceType) Announce(addr *Addr, netAddr net.Addr) (announcer, error) {
	serviceName, err := mdnsServiceName(addr.pub, "tcp")
	if err != nil {
		return nil, err
	}

	var port int
	var ip net.IP
	switch a := netAddr.(type) {
	case *net.TCPAddr:
		ip = a.IP
		port = a.Port
	default:
		return nil, fmt.Errorf("Can't announce connection of type %T", a)
	}

	log.Printf("announcing %v on tcp via mdns as ip %v\n", addr, ip)

	service, err := mdns.NewMDNSService(
		serviceName,
		serviceName,
		"",
		"",
		port,
		[]net.IP{ip},
		nil,
	)
	if err != nil {
		return nil, err
	}

	s, err := mdns.NewServer(
		&mdns.Config{
			Zone: service,
		},
	)
	if err != nil {
		return nil, err
	}

	return &mdnsTCPAnnouncer{s}, nil

}

func (at mdnsTCPAnnounceType) CanAnnounce(netAddr net.Addr) bool {
	switch netAddr.(type) {
	case *net.TCPAddr:
		return true
	default:
		return false
	}
}

type mdnsTCPAnnouncer struct {
	mdnsServer *mdns.Server
}

func (a *mdnsTCPAnnouncer) Close() error {
	return a.mdnsServer.Shutdown()

}
