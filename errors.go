package connect

import (
	"strings"
)

type multiError struct {
	Prefix string
	Errs   []error
}

func (me multiError) Error() string {
	var errStrings []string
	for _, es := range me.Errs {
		errStrings = append(errStrings, es.Error())
	}
	return me.Prefix + ": " + strings.Join(errStrings, ", ")
}
