package connect

import (
	"errors"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/nictuku/dht"
)

type dhtTCPAnnounceType struct {
	routers string
}

func (at dhtTCPAnnounceType) Announce(addr *Addr, netAddr net.Addr) (announcer, error) {
	sn, err := dhtServiceName(addr.pub, "tcp")
	if err != nil {
		return nil, err
	}
	conf := dht.NewConfig()
	conf.SaveRoutingTable = false
	// use same port for udp dht as we use for tcp for the node.
	switch a := netAddr.(type) {
	case *net.TCPAddr:
		conf.Port = a.Port
	default:
		return nil, fmt.Errorf("Can't announce connection of type %T", a)
	}

	log.Printf("announcing %v on tcp via dht\n", addr)

	if at.routers != "" {
		conf.DHTRouters = at.routers
	}

	d, err := dht.New(conf)
	if err != nil {
		return nil, err
	}

	if err := d.Start(); err != nil {
		return nil, err
	}

	ann := &dhtTCPAnnouncer{d, sn, make(chan struct{}), make(chan struct{})}

	go ann.run()

	return ann, nil
}

func (at dhtTCPAnnounceType) CanAnnounce(netAddr net.Addr) bool {
	switch netAddr.(type) {
	case *net.TCPAddr:
		return true
	default:
		return false
	}
}

type dhtTCPAnnouncer struct {
	d *dht.DHT

	sn string

	closing chan struct{}
	closed  chan struct{}
}

func (a *dhtTCPAnnouncer) run() {
	defer close(a.closed)

	a.requestPeers()

	tick := time.NewTicker(30 * time.Second)
	for {
		select {
		case <-tick.C:
			a.requestPeers()
		case <-a.closing:
			tick.Stop()
			a.d.Stop()
			return
		case <-a.d.PeersRequestResults:
			// drop results.
		}
	}
}

func (a *dhtTCPAnnouncer) requestPeers() {
	a.d.PeersRequest(a.sn, true)
}

func (a *dhtTCPAnnouncer) Close() error {
	select {
	case a.closing <- struct{}{}:
		<-a.closed
		return nil
	case <-a.closed:
		return errors.New("already closed")
	}
}
