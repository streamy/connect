package connect

import (
	"context"
	"crypto"
	"encoding/json"
	"log"
	"net"
	"time"

	"gitlab.com/streamy/anno"
)

func newBcTCPResolver() resolver {

	res := &bcTCPResolver{make(chan *resolveReq)}

	go res.loop()

	return res
}

func (d *bcTCPResolver) loop() {
	errs := make(chan error, 1)
	discovered := make(chan anno.Discovered)

	m := make(map[string][]*resolveReq)

	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		errs <- anno.Discover(ctx, anno.DiscoverConf{appID, udpAnnouncePort}, discovered)
	}()

	var err error
	for err == nil {
		select {
		case e := <-errs:
			err = e
			errs = nil
		case dis := <-discovered:
			var ad annoData
			e := json.Unmarshal(dis.Data, &ad)
			if e != nil {
				err = e
			} else {
				if ad.Type == "tcp" {
					listeners := m[ad.NodeID]
					for _, l := range listeners {
						select {
						case <-l.ctx.Done():
						case l.resp <- &net.TCPAddr{IP: dis.Address, Port: ad.Port}:
						}
					}
				}
			}
		case req := <-d.reqs:
			existing := m[req.pk]
			existing = append(existing, req)
			m[req.pk] = existing
		}
	}

	log.Println("resolved entering error state")

	cancel()

	for {
		select {
		case req := <-d.reqs:
			req.errs <- err
		}
	}

}

type bcTCPResolver struct {
	reqs chan *resolveReq
}

type resolveReq struct {
	pk   string
	resp chan net.Addr
	errs chan error
	ctx  context.Context
}

func (d *bcTCPResolver) resolve(ctx context.Context, pk crypto.PublicKey, resolved chan net.Addr) error {

	pks, err := SerialisePublic(pk)
	if err != nil {
		return err
	}

	tick := time.NewTicker(3 * time.Second)
	defer tick.Stop()

	resps := make(chan net.Addr)
	req := &resolveReq{pk: pks, resp: resps, errs: make(chan error, 1), ctx: ctx}
	d.reqs <- req

	for {
		select {
		case err := <-req.errs:
			return err
		case addr := <-req.resp:
			select {
			case resolved <- addr:
			case <-ctx.Done():
				return nil
			}
		case <-ctx.Done():
			return nil
		}

	}
}
