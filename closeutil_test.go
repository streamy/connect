package connect

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCloseNothing(t *testing.T) {
	assert := assert.New(t)
	assert.NoError(closeAll())
}

func TestCloseNils(t *testing.T) {
	assert := assert.New(t)
	assert.NoError(closeAll(nil, nil, nil))
}
