package connect

import (
	"errors"
	"net"
	"time"
)

type wrappedConn struct {
	Conn
}

func (fc wrappedConn) SetDeadline(t time.Time) error {
	return errors.New("deadline not implemented")
}

func (fc wrappedConn) SetReadDeadline(t time.Time) error {
	return errors.New("deadline not implemented")
}

func (fc wrappedConn) SetWriteDeadline(t time.Time) error {
	return errors.New("deadline not implemented")
}

func wrapConn(rwc Conn) net.Conn {
	switch x := rwc.(type) {
	case net.Conn:
		return x
	default:
		return wrappedConn{rwc}
	}
}
