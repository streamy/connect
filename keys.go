package connect

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"encoding/base64"
	"errors"
	"fmt"
	"math/big"
	"strings"

	"golang.org/x/crypto/ed25519"
)

// SerialiseSigner marshals a crypto.Signer into a string
func SerialiseSigner(signer crypto.Signer) (string, error) {
	if signer == nil {
		return "", errors.New("signer must not be nil")
	}
	switch t := signer.(type) {
	case *ecdsa.PrivateKey:
		switch t.Curve {
		case elliptic.P224():
			return "ecdsa:p224:" + encodeToString(t.D.Bytes()), nil
		case elliptic.P256():
			return "ecdsa:p256:" + encodeToString(t.D.Bytes()), nil
		case elliptic.P384():
			return "ecdsa:p384:" + encodeToString(t.D.Bytes()), nil
		case elliptic.P521():
			return "ecdsa:p521:" + encodeToString(t.D.Bytes()), nil
		default:
			return "", fmt.Errorf("unhandled curve: %T", t.Curve)
		}
	case ed25519.PrivateKey:
		return "ed25519:" + encodeToString(t), nil
	default:
		return "", fmt.Errorf("SerialiseSigner : unhandled key type: %T", t)
	}
}

// DeserialiseSigner unmarshals a string encoded signer into a crypto.Signer
func DeserialiseSigner(serialised string) (crypto.Signer, error) {

	spl := strings.SplitN(serialised, ":", 2)
	if len(spl) != 2 {
		return nil, errors.New("z malformed private key string")
	}

	switch spl[0] {
	case "ecdsa":
		sub := strings.Split(spl[1], ":")
		if len(sub) != 2 {
			return nil, errors.New("y malformed private key string")
		}
		var curve elliptic.Curve

		switch sub[0] {
		case "p224":
			curve = elliptic.P224()
		case "p256":
			curve = elliptic.P256()
		case "p384":
			curve = elliptic.P384()
		case "p521":
			curve = elliptic.P521()
		default:
			return nil, fmt.Errorf("unknown curve %v", sub[0])
		}
		bytes, err := decodeFromString(sub[1])
		if err != nil {
			return nil, err
		}
		bi := big.NewInt(0).SetBytes(bytes)

		priv := &ecdsa.PrivateKey{
			D: bi,
			PublicKey: ecdsa.PublicKey{
				Curve: curve,
			},
		}
		priv.PublicKey.X, priv.PublicKey.Y = curve.ScalarBaseMult(bi.Bytes())

		return priv, nil
	case "ed25519":
		data, err := decodeFromString(spl[1])
		if err != nil {
			return nil, err
		}
		if len(data) != ed25519.PrivateKeySize {
			return nil, errors.New("malformed public key string")
		}
		return ed25519.PrivateKey(data), nil

	default:
		return nil, fmt.Errorf("unsupported key type '%s'", spl[0])
	}

}

// SerialisePublic marshals a crypto.PublicKey into a string
func SerialisePublic(public crypto.PublicKey) (string, error) {
	switch k := public.(type) {
	case *ecdsa.PublicKey:
		switch k.Curve {
		case elliptic.P224():
			return "ecdsa:p224:" + encodeToString(elliptic.Marshal(k.Curve, k.X, k.Y)), nil
		case elliptic.P256():
			return "ecdsa:p256:" + encodeToString(elliptic.Marshal(k.Curve, k.X, k.Y)), nil
		case elliptic.P384():
			return "ecdsa:p384:" + encodeToString(elliptic.Marshal(k.Curve, k.X, k.Y)), nil
		case elliptic.P521():
			return "ecdsa:p521:" + encodeToString(elliptic.Marshal(k.Curve, k.X, k.Y)), nil
		default:
			return "", fmt.Errorf("Unhandled curve %v", k.Curve)
		}
	case ed25519.PublicKey:
		return "ed25519:" + encodeToString(k), nil
	default:
		return "", fmt.Errorf("SerialisePublic: unhandled key type: %T", k)
	}
}

// DeserialisePublic unmarshals a string encoded key into a crypto.PublicKey
func DeserialisePublic(pubStr string) (crypto.PublicKey, error) {
	spl := strings.SplitN(pubStr, ":", 2)
	if len(spl) != 2 {
		return nil, errors.New("malformed public key string")
	}
	switch spl[0] {
	case "ecdsa":
		sub := strings.Split(spl[1], ":")
		if len(sub) != 2 {
			return nil, errors.New("malformed public key string")
		}
		var curve elliptic.Curve
		switch sub[0] {
		case "p224":
			curve = elliptic.P224()
		case "p256":
			curve = elliptic.P256()
		case "p384":
			curve = elliptic.P384()
		case "p521":
			curve = elliptic.P521()
		default:
			return nil, fmt.Errorf("unknown curve %v", sub[0])
		}
		bytes, err := decodeFromString(sub[1])
		if err != nil {
			return nil, err
		}
		pubk := &ecdsa.PublicKey{
			Curve: curve,
		}

		pubk.X, pubk.Y = elliptic.Unmarshal(curve, bytes)
		if pubk.X == nil {
			return nil, errors.New("malformed public key string")
		}

		return pubk, nil

	case "ed25519":
		data, err := decodeFromString(spl[1])
		if err != nil {
			return nil, errors.New("malformed public key string")
		}
		if len(data) != ed25519.PublicKeySize {
			return nil, errors.New("malformed public key string")
		}
		return ed25519.PublicKey(data), nil
	default:
		return nil, errors.New("malformed public key string")
	}
}

func encodeToString(b []byte) string {
	return base64.URLEncoding.EncodeToString(b)
}

func decodeFromString(s string) ([]byte, error) {
	return base64.URLEncoding.DecodeString(s)

}
