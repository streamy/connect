package connect

import (
	"fmt"
	"os"
	"runtime"
	"runtime/pprof"
	"testing"
	"time"
)

func TestMain(m *testing.M) {
	gr := runtime.NumGoroutine()
	exitCode := m.Run()
	for x := 0; x < 10 && runtime.NumGoroutine() != gr; x++ {
		time.Sleep(20 * time.Millisecond)
	}
	if runtime.NumGoroutine() != gr {
		if err := pprof.Lookup("goroutine").WriteTo(os.Stdout, 2); err != nil {
			fmt.Printf("error writing goroutine profile : \n%v\n", err)
		}
		fmt.Fprintf(os.Stdout, "\nFailed due to lingering goroutines\n")
		os.Exit(-1)
	}
	os.Exit(exitCode)
}
