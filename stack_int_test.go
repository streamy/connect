package connect

import (
	"context"
	"crypto"
	"errors"
	"io"
	"io/ioutil"
	"net"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestMinimalTCP(t *testing.T) {

	tlClient, err := newTCPTransportListener()
	if err != nil {
		t.Fatal(err)
	}

	tlServer, err := newTCPTransportListener()
	if err != nil {
		t.Fatal(err)
	}

	testMinimal(t, tlClient, &tcpTransportDialler{}, tlServer, &tcpTransportDialler{})
}

func TestMinimalUTP(t *testing.T) {

	tlClient, err := newUTPTransportListener()
	if err != nil {
		t.Fatal(err)
	}

	tlServer, err := newUTPTransportListener()
	if err != nil {
		t.Fatal(err)
	}

	testMinimal(t, tlClient, &utpTransportDialler{}, tlServer, &utpTransportDialler{})

}

func testMinimal(t *testing.T, tlClient transportListener, tdClient transportDialler, tlServer transportListener, tdServer transportDialler) {
	assert := assert.New(t)

	clientKey := newSigner()
	serverKey := newSigner()

	sar := newStaticAnnouncerResolver()

	// client
	hsClient, err := newDummyHandshaker(clientKey)
	if err != nil {
		t.Fatal(err)
	}

	clientPeer, err := startPeer(clientKey, sar, sar, hsClient, []transportListener{tlClient}, tdClient)
	assert.NoError(err)

	// server
	hsServer, err := newDummyHandshaker(serverKey)
	if err != nil {
		t.Fatal(err)
	}

	serverPeer, err := startPeer(serverKey, sar, sar, hsServer, []transportListener{tlServer}, tdServer)
	assert.NoError(err)

	defer func() {
		assert.NoError(clientPeer.Close())
		assert.NoError(serverPeer.Close())
	}()

	open := make(chan struct{})

	serverErr := make(chan error)
	go func() {
		var scon io.ReadWriteCloser
		scon, err := serverPeer.Accept()
		if err != nil {
			serverErr <- err
			return
		}
		open <- struct{}{}
		serverErr <- scon.Close()

	}()

	ccon, err := clientPeer.DialContext(context.Background(), &Addr{serverKey.Public()})
	assert.NoError(err)

	<-open

	data, err := ioutil.ReadAll(ccon)
	assert.NoError(err)
	assert.Equal(0, len(data))

	assert.NoError(ccon.Close())

	assert.NoError(<-serverErr)
}

func TestIntMinimal(t *testing.T) {
	assert := assert.New(t)

	clientKey := newSigner()
	serverKey := newSigner()

	clientPeer, err := StartPeer(clientKey, nil)
	if err != nil {
		t.Fatal(err)
	}
	serverPeer, err := StartPeer(serverKey, nil)
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		assert.NoError(clientPeer.Close())
		assert.NoError(serverPeer.Close())
	}()

	open := make(chan struct{})

	serverErr := make(chan error)
	go func() {
		var scon io.ReadWriteCloser
		scon, err := serverPeer.Accept()
		if err != nil {
			serverErr <- err
			return
		}
		open <- struct{}{}
		serverErr <- scon.Close()

	}()

	ccon, err := clientPeer.DialContext(context.Background(), &Addr{serverKey.Public()})
	assert.NoError(err)

	<-open

	data, err := ioutil.ReadAll(ccon)
	assert.NoError(err)
	assert.Equal(0, len(data))

	assert.NoError(ccon.Close())

	assert.NoError(<-serverErr)
}

func TestEcho(t *testing.T) {
	assert := assert.New(t)

	clientKey := newSigner()
	serverKey := newSigner()

	clientPeer, err := StartPeer(clientKey, nil)
	if err != nil {
		t.Fatal(err)
	}
	serverPeer, err := StartPeer(serverKey, nil)
	if err != nil {
		t.Fatal(err)
	}

	defer func() {
		assert.NoError(clientPeer.Close())
		assert.NoError(serverPeer.Close())
	}()

	serverErr := make(chan error)
	go func() {
		var scon io.ReadWriteCloser
		scon, err := serverPeer.Accept()
		if err != nil {
			serverErr <- err
			return
		}
		for {
			// Short buffer to force multiple reads/write cycles
			buf := make([]byte, 4)

			var i int
			i, err = scon.Read(buf)
			if err == io.EOF {
				break
			}
			if err != nil {
				serverErr <- err
				return
			}
			var j int
			j, err = scon.Write(buf[0:i])
			if err != nil {
				serverErr <- err
				return
			}
			if j != i {
				serverErr <- errors.New("bad write length")
				return
			}
		}
		serverErr <- scon.Close()

	}()

	ccon, err := clientPeer.DialContext(context.Background(), &Addr{serverKey.Public()})
	assert.NoError(err)

	msg := []byte("hello world")
	_, err = ccon.Write(msg)
	assert.NoError(err)

	rbuf := make([]byte, 1024)
	i, err := io.ReadAtLeast(ccon, rbuf, len(msg))
	assert.NoError(err)
	assert.Equal(msg, rbuf[0:i])

	assert.NoError(ccon.Close())

	assert.NoError(<-serverErr)

}

func TestDialWithoutAccept(t *testing.T) {
	assert := assert.New(t)

	clientKey := newSigner()
	serverKey := newSigner()

	sar := newStaticAnnouncerResolver()

	// client
	transClient, err := newTCPTransportListener()
	if err != nil {
		t.Fatal(err)
	}

	hsClient, err := newTLSHandshaker(clientKey)
	if err != nil {
		t.Fatal(err)
	}

	clientPeer, err := startPeer(clientKey, sar, sar, hsClient, []transportListener{transClient}, &tcpTransportDialler{})
	assert.NoError(err)

	// server
	transServer, err := newTCPTransportListener()
	if err != nil {
		t.Fatal(err)
	}

	hsServer, err := newTLSHandshaker(serverKey)
	if err != nil {
		t.Fatal(err)
	}

	serverPeer, err := startPeer(serverKey, sar, sar, hsServer, []transportListener{transServer}, &tcpTransportDialler{})
	assert.NoError(err)

	defer func() {
		assert.NoError(clientPeer.Close())
		assert.NoError(serverPeer.Close())
	}()

	ctx, cancel := context.WithCancel(context.Background())

	clientConnected := make(chan struct{}, 1)
	clientErr := make(chan error, 1)
	go func() {
		c, err := clientPeer.DialContext(ctx, &Addr{serverKey.Public()})
		if err != nil {
			clientErr <- err
			return
		}
		_ = c.Close()
		clientConnected <- struct{}{}
	}()

	select {
	case <-time.NewTimer(1000 * time.Millisecond).C:
	case err := <-clientErr:
		t.Errorf("Unexpected Dial error : %v", err)
	case <-clientConnected:
		t.Error("client should not have connected")
	}
	cancel()

}

func TestDialUnresolvable(t *testing.T) {

	assert := assert.New(t)

	s := newSigner()
	r := &failResolver{}
	at := &nullAnnouncerType{}
	h := &panicHandshaker{}
	tr := &mockTransport{make(chan struct{})}

	peer, err := startPeer(s, r, at, h, []transportListener{tr}, tr)
	assert.NoError(err)

	conn, err := peer.DialContext(context.Background(), &Addr{s.Public()})
	assert.NotNil(err)
	assert.Nil(conn)

	assert.NoError(peer.Close())

}

func TestCloseWhileAttemptingAccept(t *testing.T) {
	assert := assert.New(t)

	s := newSigner()
	r := &failResolver{}
	at := &nullAnnouncerType{}
	h := &panicHandshaker{}
	tr := &mockTransport{make(chan struct{})}

	peer, err := startPeer(s, r, at, h, []transportListener{tr}, tr)
	assert.NoError(err)

	errs := make(chan error)
	go func() {
		conn, err := peer.Accept()
		assert.Nil(conn)
		errs <- err
	}()

	assert.NoError(peer.Close())
	assert.Equal(ErrConnectionClosed, <-errs)
}

func newStaticAnnouncerResolver() *staticAnnouncerResolver {
	return &staticAnnouncerResolver{make(map[string]net.Addr)}
}

type staticAnnouncerResolver struct {
	entries map[string]net.Addr
}

func (sr *staticAnnouncerResolver) resolve(ctx context.Context, pk crypto.PublicKey, addrs chan net.Addr) error {
	ser, err := SerialisePublic(pk)
	if err != nil {
		return err
	}
	a, ok := sr.entries[ser]
	if ok {
		addrs <- a
	}
	<-ctx.Done()
	return ctx.Err()
}

func (sr *staticAnnouncerResolver) Announce(a *Addr, t net.Addr) (announcer, error) {
	ser, err := SerialisePublic(a.pub)
	if err != nil {
		return nil, err
	}
	sr.entries[ser] = t
	return sr, nil
}

func (sr *staticAnnouncerResolver) CanAnnounce(netAddr net.Addr) bool {
	return true
}

func (sr *staticAnnouncerResolver) Close() error {
	return nil
}

type panicHandshaker struct{}

func (ph *panicHandshaker) handshakeClient(Conn, crypto.PublicKey) (Conn, error) {
	panic("should not reach here")
}
func (ph *panicHandshaker) handshakeServer(Conn) (Conn, error) {
	panic("should not reach here")
}

type mockTransport struct {
	closed chan struct{}
}

func (mt *mockTransport) Accept() (Conn, error) {
	<-mt.closed
	return nil, errTransportClosed
}
func (mt *mockTransport) LocalAddr() net.Addr {
	ta, err := net.ResolveTCPAddr("tcp", "1.2.3.4:567")
	if err != nil {
		panic(err)
	}
	return ta
}
func (mt *mockTransport) Dial(net.Addr) (Conn, error) {
	panic("should not reach here")
}
func (mt *mockTransport) CanDial(net.Addr) bool {
	panic("should not reach here")
}
func (mt *mockTransport) Close() error {
	close(mt.closed)
	return nil
}

type nullAnnouncerType struct{}

func (nat *nullAnnouncerType) Announce(*Addr, net.Addr) (announcer, error) {
	return &nullAnnouncer{}, nil
}
func (nat *nullAnnouncerType) CanAnnounce(net.Addr) bool {
	return true
}

type nullAnnouncer struct{}

func (na *nullAnnouncer) Close() error { return nil }

type failResolver struct{}

func (fr *failResolver) resolve(ctx context.Context, pk crypto.PublicKey, resolved chan net.Addr) error {
	return errors.New("failResolver failed to resolve")
}
