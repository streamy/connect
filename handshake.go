package connect

import (
	"crypto"
	"errors"
)

type handshaker interface {
	handshakeClient(Conn, crypto.PublicKey) (Conn, error)
	handshakeServer(Conn) (Conn, error)
}

var (
	errHsConnectionClosed = errors.New("connection closed")
	errHsConnectionReset  = errors.New("connection reset by peer")
)
