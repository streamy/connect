package connect

import (
	"context"
	"crypto"
	"errors"
	"io"
	"log"
	"net"
	"sync"
)

// Peer represents a node on the network
type Peer struct {
	s crypto.Signer

	r   resolver
	a   []announcer
	h   handshaker
	tls []transportListener
	td  transportDialler

	newTransportConns chan Conn
	newConns          chan Conn

	transportWG sync.WaitGroup

	closed chan struct{}
}

// PeerConfig allows customising the behaviour of a Peer
type PeerConfig struct {
	ResolveMDNS  bool
	AnnounceMDNS bool
	ResolveDHT   bool
	AnnounceDHT  bool
	ResolveUDP   bool
	AnnounceUDP  bool
	UseTCP       bool
	UseUTP       bool
}

// DefaultPeerConfig returns a new configuration with the default values set
func DefaultPeerConfig() *PeerConfig {
	return &PeerConfig{true, true, true, true, true, true, true, true}
}

// StartPeer starts a Peer and makes it ready for use
func StartPeer(s crypto.Signer, conf *PeerConfig) (*Peer, error) {
	if conf == nil {
		conf = DefaultPeerConfig()
	}

	// transport listeners
	var tls []transportListener
	if conf.UseTCP {
		tt, err := newTCPTransportListener()
		if err != nil {
			return nil, err
		}
		tls = append(tls, tt)
	}

	if conf.UseUTP {
		tu, err := newUTPTransportListener()
		if err != nil {
			return nil, err
		}
		tls = append(tls, tu)
	}

	// resolvers
	r := &multiResolver{}
	if conf.ResolveDHT {
		if conf.UseTCP {
			r.resolvers = append(r.resolvers, newDHTTCPResolver(""))
		}
		if conf.UseUTP {
			r.resolvers = append(r.resolvers, newDHTUTPResolver(""))
		}
	}
	if conf.ResolveUDP {
		if conf.UseTCP {
			r.resolvers = append(r.resolvers, newBcTCPResolver())
		}
		if conf.UseUTP {
			// TODO
		}
	}
	if conf.ResolveMDNS {
		if conf.UseTCP {
			r.resolvers = append(r.resolvers, newMDNSTCPResolver())
		}
		if conf.UseUTP {
			r.resolvers = append(r.resolvers, newMDNSUTPResolver())
		}
	}
	//h := &sshHandshaker{s}
	h, err := newTLSHandshaker(s)
	if err != nil {
		return nil, err
	}

	// announcers
	at := multiAnnounceType{}
	if conf.AnnounceDHT {
		if conf.UseTCP {
			at.types = append(at.types, dhtTCPAnnounceType{})
		}
		if conf.UseUTP {
			at.types = append(at.types, dhtUTPAnnounceType{})
		}
	}
	if conf.AnnounceUDP {
		if conf.UseTCP {
			at.types = append(at.types, bcTCPAnnounceType{})
		}
		if conf.UseUTP {
			// TODO
		}
	}
	if conf.AnnounceMDNS {
		if conf.UseTCP {
			at.types = append(at.types, mdnsTCPAnnounceType{})
		}
		if conf.UseUTP {
			at.types = append(at.types, mdnsUTPAnnounceType{})
		}
	}

	// transport diallers
	var tds []transportDialler
	if conf.UseTCP {
		tds = append(tds, &tcpTransportDialler{})
	}
	if conf.UseUTP {
		tds = append(tds, &utpTransportDialler{})
	}
	return startPeer(s, r, at, h, tls, newTransportDiallerMulti(tds...))
}

func startPeer(s crypto.Signer, r resolver, at announceType, h handshaker, tls []transportListener, td transportDialler) (*Peer, error) {

	p := &Peer{
		r:                 r,
		h:                 h,
		tls:               tls,
		td:                td,
		s:                 s,
		newTransportConns: make(chan Conn, 16),
		newConns:          make(chan Conn, 16),

		closed: make(chan struct{}),
	}

	for _, tl := range tls {
		// announce
		ta := tl.LocalAddr()
		ann, err := at.Announce(&Addr{s.Public()}, ta)
		if err != nil {
			return nil, err
		}
		p.a = append(p.a, ann)

		// accept
		p.transportWG.Add(1)
		go func(tl transportListener) {
			defer p.transportWG.Done()
			for {
				rwc, err := tl.Accept()
				if err != nil {
					if err == errTransportClosed {
						return
					}
					panic(err)
				}
				p.newTransportConns <- rwc
			}
		}(tl)
	}

	go func() {
		defer close(p.newConns)
		for tc := range p.newTransportConns {
			conn, err := p.h.handshakeServer(tc)
			if err != nil {
				log.Printf("server: handshake failed, closing connection : %v\n", err)
				_ = tc.Close()
			} else {
				select {
				case p.newConns <- conn:
				default:
					log.Println("server: incoming connections not being accepted fast enough. Closing")
					_ = tc.Close()
				}
			}
		}
	}()

	return p, nil
}

// Close shuts down a peer, terminating all open connections
func (p *Peer) Close() (err error) {
	toClose := []io.Closer{}
	for _, cl := range p.a {
		toClose = append(toClose, cl)
	}
	for _, cl := range p.tls {
		toClose = append(toClose, cl)
	}
	err = closeAll(toClose...)
	p.transportWG.Wait()
	// Close pending incoming transport conns
	close(p.newTransportConns)
	for tc := range p.newTransportConns {
		_ = tc.Close()
	}
	// .. and any handshaken connections not accepted yet
	for c := range p.newConns {
		_ = c.Close()
	}
	// TODO : We don't close existing open connections, but we should
	close(p.closed)
	return
}

// Dial attempts a connection to the given Addr
func (p *Peer) Dial(a *Addr) (Conn, error) {
	return p.DialContext(context.Background(), a)
}

// Dial attempts a connection to the given Addr
func (p *Peer) DialContext(ctx context.Context, a *Addr) (Conn, error) {

	candidateAddrs := make(chan net.Addr)

	errs := make(chan error, 1)

	var wg sync.WaitGroup
	wg.Add(1)
	defer wg.Wait()

	ctx, cancel := context.WithCancel(ctx)
	defer cancel()

	go func() {
		defer close(candidateAddrs)
		defer wg.Done()
		if err := p.r.resolve(ctx, a.pub, candidateAddrs); err != nil {
			errs <- err
		}
	}()

	ca := candidateAddrs
	for {
		select {
		case err := <-errs:
			return nil, err
		case addr, ok := <-ca:
			if !ok {
				ca = nil
				continue
			}
			c, err := p.td.Dial(addr)
			if err != nil {
				log.Printf("error attempting connection to '%v' skipping: %s", addr, err.Error())
				continue
			}
			con, err := p.h.handshakeClient(c, a.pub)
			if err != nil {
				log.Printf("error attempting security handshake with '%v' skipping: %s", addr, err.Error())
				_ = c.Close()
				continue
			}

			// We add our own "handshake" by reading 1 byte. The server writes this byte
			// to indicate that the connection has been accepted at the Peer level.
			buf := make([]byte, 1)
			_, err = con.Read(buf)
			if err != nil {
				log.Printf("error attempting accept handshake with '%v' from '%s' skipping: %s", addr, con.LocalAddr(), err.Error())
				_ = con.Close()
				continue
			}
			return con, nil
		case <-ctx.Done():
			return nil, ctx.Err()
			//			return nil, errors.New("failed to connect before timeout")
		}
	}

}

// Accept waits for an incoming connection and returns it to the caller
func (p *Peer) Accept() (Conn, error) {
	for {
		select {
		case con, ok := <-p.newConns:
			if !ok {
				return nil, ErrConnectionClosed
			}
			if _, err := con.Write([]byte("0")); err == nil {
				return con, nil
			} else {
				log.Printf("Accept failed to write handshake byte so closing: %v\n", err)
				_ = con.Close()
			}
		case <-p.closed:
			return nil, ErrConnectionClosed
		}
	}
}

// Addr represents the address of a Peer
type Addr struct {
	pub crypto.PublicKey
}

func (a *Addr) String() string {
	s, err := SerialisePublic(a.pub)
	if err != nil {
		panic("this is a bug")
	}
	return s
}

func (a *Addr) Network() string {
	return "streamy"
}

func AddrFromPublicKey(pub crypto.PublicKey) (*Addr, error) {

	// ensure we can handle the key
	_, err := SerialisePublic(pub)
	if err != nil {
		return nil, err
	}

	return &Addr{pub}, nil
}

// Conn is a stream oriented connection. Note that this ia s sub-interface of net.Conn
type Conn interface {
	io.ReadWriteCloser
	RemoteAddr() net.Addr
	LocalAddr() net.Addr
}

type resolver interface {
	resolve(ctx context.Context, pubKey crypto.PublicKey, resolved chan net.Addr) error
}

type announcer interface {
	Close() error
}

type announceType interface {
	Announce(*Addr, net.Addr) (announcer, error)
	CanAnnounce(net.Addr) bool
}

type transportDialler interface {
	Dial(net.Addr) (Conn, error)
	CanDial(net.Addr) bool
}

type transportListener interface {
	Accept() (Conn, error)
	LocalAddr() net.Addr
	Close() error
}

var errTransportClosed = errors.New("Transport closed")

var (
	// ErrConnectionClosed indicates use of a closed connection
	ErrConnectionClosed = errors.New("connection closed")
)
