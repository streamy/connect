package connect

import (
	"bytes"
	"crypto"
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"errors"
	"fmt"
	"math/big"
	"net"
	"time"
)

var (
	snMax       = big.NewInt(0).Lsh(big.NewInt(1), 128)
	fixedDomain = "streamy"
)

func newTLSHandshaker(priv crypto.Signer) (*tlsHandshaker, error) {
	cert, err := selfSignedTLSCert(priv)
	if err != nil {
		return nil, err
	}
	return &tlsHandshaker{priv, cert}, nil
}

type tlsHandshaker struct {
	priv crypto.Signer
	cert tls.Certificate
}

func (hs *tlsHandshaker) handshakeClient(conn Conn, serverKey crypto.PublicKey) (Conn, error) {
	serverCert, err := signedCert(hs.priv, serverKey)
	if err != nil {
		return nil, err
	}

	certPool := x509.NewCertPool()
	certPool.AddCert(serverCert)

	conf := &tls.Config{
		RootCAs:      certPool,
		Certificates: []tls.Certificate{hs.cert},
		//DNSNames:     []string{fixedDomain},
		ServerName: fixedDomain,
	}
	c := tls.Client(wrapConn(conn), conf)
	if err := c.Handshake(); err != nil {
		_ = c.Close()
		return nil, err
	}
	certs := c.ConnectionState().PeerCertificates
	if len(certs) != 1 {
		_ = c.Close()
		return nil, errors.New("unexpected number of certs")
	}
	//pubKey := certs[0].PublicKey TODO: assert that this is the expected key? Should not be possible, but worth checking?
	return &tlsConn{c, &Addr{hs.priv.Public()}, &Addr{serverKey}}, nil

}

func (hs *tlsHandshaker) handshakeServer(conn Conn) (Conn, error) {
	conf := &tls.Config{
		Certificates: []tls.Certificate{hs.cert},
		ClientAuth:   tls.RequireAnyClientCert,
	}
	c := tls.Server(wrapConn(conn), conf)
	if err := c.Handshake(); err != nil {
		return nil, err
	}
	certs := c.ConnectionState().PeerCertificates
	if len(certs) != 1 {
		_ = c.Close()
		return nil, errors.New("Certs not as expected")
	}

	pk := certs[0].PublicKey

	return &tlsConn{c, &Addr{hs.priv.Public()}, &Addr{pk}}, nil
}

type tlsConn struct {
	conn       *tls.Conn
	localAddr  *Addr
	remoteAddr *Addr
}

func (c *tlsConn) Read(buf []byte) (int, error) {
	return c.conn.Read(buf)
}

func (c *tlsConn) Write(buf []byte) (int, error) {
	i, err := c.conn.Write(buf)
	if err != nil && (err.Error() == "crypto/tls: use of closed connection" || err.Error() == "tls: use of closed connection") {

		err = errHsConnectionClosed
	}
	return i, err
}

func (c *tlsConn) Close() error {
	err := c.conn.Close()
	if err != nil && (err.Error() == "crypto/tls: use of closed connection" || err.Error() == "tls: use of closed connection") {
		err = errHsConnectionClosed
	}
	return err
}

func (c *tlsConn) LocalAddr() net.Addr {
	return c.localAddr
}

func (c *tlsConn) RemoteAddr() net.Addr {
	return c.remoteAddr
}

func x509Template() (*x509.Certificate, error) {

	serialNumber, err := rand.Int(rand.Reader, snMax)
	if err != nil {
		return nil, err
	}

	return &x509.Certificate{
		SerialNumber: serialNumber,
		Subject: pkix.Name{
			Organization: []string{"org"},
		},
		NotBefore: time.Now(),
		NotAfter:  time.Now().Add(365 * 24 * time.Hour),

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth, x509.ExtKeyUsageClientAuth},
		BasicConstraintsValid: true,
		DNSNames:              []string{fixedDomain},
		IsCA:                  true,
	}, nil

}

func selfSignedTLSCert(signer crypto.Signer) (tls.Certificate, error) {
	priv, ok := signer.(*ecdsa.PrivateKey)
	if !ok {
		return tls.Certificate{}, fmt.Errorf("signer type not supported : %T", signer)
	}

	tmpl, err := x509Template()
	if err != nil {
		return tls.Certificate{}, err
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, tmpl, tmpl, priv.Public(), priv)
	if err != nil {
		return tls.Certificate{}, err
	}

	var certBuf bytes.Buffer
	err = pem.Encode(&certBuf, &pem.Block{Type: "CERTIFICATE", Bytes: derBytes})
	if err != nil {
		return tls.Certificate{}, err
	}

	b, err := x509.MarshalECPrivateKey(priv)
	if err != nil {
		return tls.Certificate{}, err
	}

	var bufKey bytes.Buffer
	pemKey := &pem.Block{Type: "EC PRIVATE KEY", Bytes: b}
	err = pem.Encode(&bufKey, pemKey)
	if err != nil {
		return tls.Certificate{}, err
	}

	return tls.X509KeyPair(certBuf.Bytes(), bufKey.Bytes())
}

func signedCert(signer crypto.Signer, pub crypto.PublicKey) (*x509.Certificate, error) {
	ecPub := pub.(*ecdsa.PublicKey)

	tmpl, err := x509Template()
	if err != nil {
		return nil, err
	}

	derBytes, err := x509.CreateCertificate(rand.Reader, tmpl, tmpl, ecPub, signer)
	if err != nil {
		return nil, err
	}

	crt, err := x509.ParseCertificate(derBytes)
	if err != nil {
		return nil, err
	}
	return crt, nil

}
