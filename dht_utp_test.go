package connect

import (
	"context"
	"net"
	"sync"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestUTPAnnounceResolve(t *testing.T) {
	assert := assert.New(t)

	publicKey := newSigner().Public()

	sn, err := dhtServiceName(publicKey, "utp")
	assert.NoError(err)
	snBytes := []byte(sn)
	t.Logf("info hash is %x\n", snBytes)

	// start router dht node
	d := runDHTRouter(4444)
	defer d.Stop()

	time.Sleep(100 * time.Millisecond)

	hostAndPort := "127.0.0.1:2222"

	ta, err := net.ResolveUDPAddr("udp", hostAndPort)
	assert.NoError(err)

	at := dhtUTPAnnounceType{routers: "localhost:4444"}
	ann, err := at.Announce(&Addr{publicKey}, ta)
	assert.NoError(err)

	defer ann.Close()

	time.Sleep(800 * time.Millisecond)

	resolver := newDHTUTPResolver("localhost:4444")

	candidates := make(chan net.Addr)

	ctx, cancel := context.WithCancel(context.Background())
	var wg sync.WaitGroup

	errs := make(chan error, 1)
	wg.Add(1)
	go func() {
		defer wg.Done()
		err := resolver.resolve(ctx, publicKey, candidates)
		if err != nil {
			errs <- err
		}
	}()

	select {
	case err := <-errs:
		t.Fatal(err)
	case candidate := <-candidates:
		assert.Equal(hostAndPort, candidate.String())
	}
	cancel()

	wg.Wait()
}
