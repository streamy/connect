package connect

import (
	"io"
	"net"
)

type multiAnnounceType struct {
	types []announceType
}

func (mat multiAnnounceType) Announce(addr *Addr, tcpAddr net.Addr) (announcer, error) {
	var announcers []announcer

	for _, at := range mat.filtered(tcpAddr) {
		announcer, err := at.Announce(addr, tcpAddr)
		if err != nil {
			for _, toClose := range announcers {
				toClose.Close()
			}
			return nil, err
		}
		announcers = append(announcers, announcer)
	}

	ret := &multiAnnouncer{announcers}
	return ret, nil
}

func (mat multiAnnounceType) CanAnnounce(netAddr net.Addr) bool {
	return len(mat.filtered(netAddr)) > 0
}

func (mat multiAnnounceType) filtered(netAddr net.Addr) []announceType {
	var f []announceType
	for _, at := range mat.types {
		if at.CanAnnounce(netAddr) {
			f = append(f, at)
		}
	}
	return f
}

type multiAnnouncer struct {
	announcers []announcer
}

func (ma *multiAnnouncer) Close() error {
	closers := make([]io.Closer, len(ma.announcers))
	for i, ann := range ma.announcers {
		closers[i] = ann
	}
	return closeAll(closers...)
}
