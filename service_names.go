package connect

import (
	"crypto"
	"crypto/sha256"
	"encoding/base32"
)

func dhtServiceName(pk crypto.PublicKey, net string) (string, error) {
	sum, err := serviceNameSum(pk, net)
	if err != nil {
		return "", err
	}
	return string(sum[0:20]), nil
}

func mdnsServiceName(pk crypto.PublicKey, net string) (string, error) {
	sum, err := serviceNameSum(pk, net)
	if err != nil {
		return "", err
	}
	return base32.StdEncoding.EncodeToString(sum)[0:16], nil
}

func serviceNameSum(pk crypto.PublicKey, net string) ([]byte, error) {
	ser, err := SerialisePublic(pk)
	if err != nil {
		return nil, err
	}
	h := sha256.New()
	if _, err := h.Write([]byte(ser)); err != nil {
		return nil, err
	}
	if _, err := h.Write([]byte(net)); err != nil {
		return nil, err
	}
	return h.Sum(nil), nil
}
