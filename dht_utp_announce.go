package connect

import (
	"errors"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/nictuku/dht"
)

type dhtUTPAnnounceType struct {
	routers string
}

func (at dhtUTPAnnounceType) Announce(addr *Addr, netAddr net.Addr) (announcer, error) {
	sn, err := dhtServiceName(addr.pub, "utp")
	if err != nil {
		return nil, err
	}
	conf := dht.NewConfig()
	conf.SaveRoutingTable = false
	// Use port +1 for DHT UDP
	switch a := netAddr.(type) {
	case *net.UDPAddr:
		conf.Port = a.Port + 1
	default:
		return nil, fmt.Errorf("Can't announce connection of type %T", a)
	}

	log.Printf("announcing %v on utp via dht\n", addr)

	if at.routers != "" {
		conf.DHTRouters = at.routers
	}

	d, err := dht.New(conf)
	if err != nil {
		return nil, err
	}

	if err := d.Start(); err != nil {
		return nil, err
	}

	ann := &dhtUTPAnnouncer{d, sn, make(chan struct{}), make(chan struct{})}

	go ann.run()

	return ann, nil
}

func (at dhtUTPAnnounceType) CanAnnounce(netAddr net.Addr) bool {
	switch netAddr.(type) {
	case *net.UDPAddr:
		return true
	default:
		return false
	}
}

type dhtUTPAnnouncer struct {
	d *dht.DHT

	sn string

	closing chan struct{}
	closed  chan struct{}
}

func (a *dhtUTPAnnouncer) run() {
	defer close(a.closed)

	a.requestPeers()

	tick := time.NewTicker(30 * time.Second)
	for {
		select {
		case <-tick.C:
			a.requestPeers()
		case <-a.closing:
			tick.Stop()
			a.d.Stop()
			return
		case <-a.d.PeersRequestResults:
			// drop results.
		}
	}
}

func (a *dhtUTPAnnouncer) requestPeers() {
	a.d.PeersRequest(a.sn, true)
}

func (a *dhtUTPAnnouncer) Close() error {
	select {
	case a.closing <- struct{}{}:
		<-a.closed
		return nil
	case <-a.closed:
		return errors.New("already closed")
	}
}
