package connect

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"net"
)

// returns a connected local tcp connection pair
func newTCPConns() (Conn, Conn) {
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		panic(err)
	}
	defer func() {
		if err = l.Close(); err != nil {
			panic(err)
		}
	}()
	addrUsed := l.(*net.TCPListener).Addr()

	sconnCh := make(chan Conn)
	go func() {
		var tcpConn Conn
		tcpConn, err := l.Accept()
		if err != nil {
			panic(err)
		}
		sconnCh <- tcpConn
	}()

	cconn, err := net.Dial("tcp", addrUsed.String())
	if err != nil {
		panic(err)
	}

	return cconn, <-sconnCh

}

func newSigner() crypto.Signer {
	return newSignerP224()
}

func newSignerP521() crypto.Signer {
	pk, err := ecdsa.GenerateKey(elliptic.P521(), rand.Reader)
	if err != nil {
		panic(err)
	}
	return pk
}

func newSignerP224() crypto.Signer {
	pk, err := ecdsa.GenerateKey(elliptic.P224(), rand.Reader)
	if err != nil {
		panic(err)
	}
	return pk
}

func newHSClientAndServer(c1, c2 Conn, clientHS, serverHS handshaker, clientKey, serverKey crypto.Signer) (Conn, Conn) {

	cmuxCh := make(chan Conn)
	go func() {

		cmux, err := clientHS.handshakeClient(c1, serverKey.Public())
		if err != nil {
			panic(err)
		}

		cmuxCh <- cmux
	}()

	smux, err := serverHS.handshakeServer(c2)
	if err != nil {
		panic(err)
	}
	return <-cmuxCh, smux
}
