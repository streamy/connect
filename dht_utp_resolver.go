package connect

import (
	"context"
	"crypto"
	"log"
	"net"
	"time"

	"github.com/nictuku/dht"
)

func newDHTUTPResolver(routers string) resolver {
	return &dhtUTPResolver{
		routers: routers,
	}
}

type dhtUTPResolver struct {
	routers string
}

func (r *dhtUTPResolver) resolve(ctx context.Context, pk crypto.PublicKey, resolved chan net.Addr) error {
	sn, err := dhtServiceName(pk, "utp")
	if err != nil {
		return err
	}
	conf := dht.NewConfig()
	conf.SaveRoutingTable = false
	if r.routers != "" {
		conf.DHTRouters = r.routers
	}

	d, err := dht.New(conf)
	if err != nil {
		return err
	}

	if err := d.Start(); err != nil {
		return err
	}

	defer d.Stop()

	reqTicker := time.NewTicker(3 * time.Second)
	defer reqTicker.Stop()

	d.PeersRequest(sn, false)

	for {
		select {
		case <-ctx.Done():
			return ctx.Err()
		case <-reqTicker.C:
			d.PeersRequest(sn, false)
		case results := <-d.PeersRequestResults:
			addresses := results[dht.InfoHash(sn)]
			for _, address := range addresses {
				addr, err := net.ResolveUDPAddr("udp", dht.DecodePeerAddress(address))
				if err != nil {
					log.Printf("error resolving address, skipping : %s", err.Error())
				} else {
					addr.Port--
					select {
					case resolved <- addr:
					case <-ctx.Done():
						// don't block returning result if we're exiting
					}
				}
			}
		}
	}
}
