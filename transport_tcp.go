package connect

import (
	"fmt"
	"net"
	"strings"
)

type tcpTransportListener struct {
	l net.Listener
	a net.Addr
}

func newTCPTransportListener() (transportListener, error) {
	l, err := net.Listen("tcp", ":0")
	if err != nil {
		return nil, err
	}
	addr := l.(*net.TCPListener).Addr()
	return &tcpTransportListener{l, addr}, nil
}

func (t *tcpTransportListener) Accept() (Conn, error) {
	con, err := t.l.Accept()
	if err != nil {
		if strings.Contains(err.Error(), "use of closed network connection") {
			err = errTransportClosed
		}
	}
	return con, err
}

func (t *tcpTransportListener) LocalAddr() net.Addr {
	return t.a
}

func (t *tcpTransportListener) Close() error {
	t.a = nil
	return t.l.Close()
}

type tcpTransportDialler struct {
}

func (t *tcpTransportDialler) Dial(addr net.Addr) (Conn, error) {
	ta, ok := addr.(*net.TCPAddr)
	if !ok {
		return nil, fmt.Errorf("can't dial addresses of type %T", addr)
	}
	return net.DialTCP("tcp", nil, ta)
}

func (t *tcpTransportDialler) CanDial(addr net.Addr) bool {
	_, ok := addr.(*net.TCPAddr)
	return ok
}
