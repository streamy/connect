package connect

import (
	"crypto"
	"encoding/binary"
	"io"
	"net"
)

type dummyHandshaker struct {
	priv crypto.Signer
}

func newDummyHandshaker(signer crypto.Signer) (*dummyHandshaker, error) {
	return &dummyHandshaker{signer}, nil
}

func (hs *dummyHandshaker) handshakeClient(conn Conn, serverKey crypto.PublicKey) (Conn, error) {
	// send our key to the server

	serStr, err := SerialisePublic(hs.priv.Public())
	if err != nil {
		return nil, err
	}
	ser := []byte(serStr)
	lenser := len(ser)

	buf := make([]byte, lenser+2)
	binary.BigEndian.PutUint16(buf, uint16(lenser))
	copy(buf[2:], ser)

	if _, err := conn.Write(buf); err != nil {
		return nil, err
	}
	return &dummyReadWriteCloser{conn, &Addr{hs.priv.Public()}, &Addr{serverKey}}, nil
}

func (hs *dummyHandshaker) handshakeServer(conn Conn) (Conn, error) {
	// read client key and blindly trust it, nice and secure like.
	var lenBuf = []byte{0, 0}
	if _, err := conn.Read(lenBuf); err != nil {
		return nil, err
	}
	len := binary.BigEndian.Uint16(lenBuf)
	buf := make([]byte, len)
	if _, err := io.ReadFull(conn, buf); err != nil {
		return nil, err
	}
	pk, err := DeserialisePublic(string(buf))
	if err != nil {
		return nil, err
	}
	return &dummyReadWriteCloser{conn, &Addr{hs.priv.Public()}, &Addr{pk}}, nil
}

type dummyReadWriteCloser struct {
	conn       Conn
	localAddr  *Addr
	remoteAddr *Addr
}

func (s *dummyReadWriteCloser) Read(buf []byte) (int, error) {
	i, err := s.conn.Read(buf)
	switch e := err.(type) {
	case *net.OpError:
		if e.Err.Error() == "read: connection reset by peer" {
			err = errHsConnectionReset
		}
	}
	return i, err
}

func (s *dummyReadWriteCloser) Write(buf []byte) (int, error) {
	i, err := s.conn.Write(buf)
	switch e := err.(type) {
	case *net.OpError:
		if e.Err.Error() == "use of closed network connection" {
			err = errHsConnectionClosed
		}
	}
	//	if err == io.EOF {
	//		err = errHsConnectionClosed
	//	}
	return i, err
}

func (s *dummyReadWriteCloser) Close() error {
	err := s.conn.Close()
	switch e := err.(type) {
	case *net.OpError:
		if e.Err.Error() == "use of closed network connection" {
			err = errHsConnectionClosed
		}
	}
	return err
}

func (s *dummyReadWriteCloser) LocalAddr() net.Addr {
	return s.localAddr
}

func (s *dummyReadWriteCloser) RemoteAddr() net.Addr {
	return s.remoteAddr
}
