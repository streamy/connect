package connect

import (
	"fmt"
	"net"

	"github.com/anacrolix/utp"
)

type utpTransport struct {
	pc *utp.Socket
}

func newUTPTransportListener() (transportListener, error) {
	pc, err := utp.NewSocket("udp", ":0")
	if err != nil {
		return nil, err
	}
	return &utpTransport{pc}, nil
}

func (t *utpTransport) Accept() (Conn, error) {
	con, err := t.pc.Accept()
	if err != nil {
		if err.Error() == "closed" {
			err = errTransportClosed
		}
	}
	return con, err
}

func (t *utpTransport) LocalAddr() net.Addr {
	return t.pc.Addr()
}

func (t *utpTransport) Close() error {
	return t.pc.CloseNow()
}

type utpTransportDialler struct{}

func (t *utpTransportDialler) CanDial(addr net.Addr) bool {
	_, ok := addr.(*net.UDPAddr)
	return ok
}

func (t *utpTransportDialler) Dial(addr net.Addr) (Conn, error) {
	if _, ok := addr.(*net.UDPAddr); !ok {
		return nil, fmt.Errorf("can't dial addresses of type %T", addr)
	}
	return utp.Dial(addr.String())
}
