package connect

import (
	"context"
	"crypto"
	"errors"
	"net"
	"sync"
)

type multiResolver struct {
	resolvers []resolver
}

func newMultiResolver(r ...resolver) resolver {
	return &multiResolver{resolvers: r}
}

func (mr *multiResolver) resolve(ctx context.Context, pk crypto.PublicKey, resolved chan net.Addr) error {
	errs := make(chan error, len(mr.resolvers))

	if len(mr.resolvers) == 0 {
		return errors.New("unable to resolve - no resolvers configured")
	}

	var wg sync.WaitGroup

	for _, r := range mr.resolvers {
		wg.Add(1)
		go func(r resolver) {
			defer wg.Done()
			err := r.resolve(ctx, pk, resolved)
			if err != nil {
				errs <- err
			}
		}(r)
	}

	wg.Wait()
	close(errs)

	return collectErrors(errs)
}

func collectErrors(errsCh <-chan error) error {
	var errs []error
	for e := range errsCh {
		errs = append(errs, e)
	}
	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errs[0]
	default:
		for _, e := range errs {
			if errs[0].Error() != e.Error() {
				return multiError{"Error resolving address", errs}
			}
		}
		return errs[0]
	}

}
