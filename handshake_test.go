package connect

import (
	"crypto"
	"fmt"
	"io"
	"io/ioutil"
	"testing"

	"github.com/stretchr/testify/assert"
)

type handshakeTestType struct {
	name  string
	newHS func(crypto.Signer) (handshaker, error)
}

var hsTestTypes = []handshakeTestType{
	{"dummy", func(s crypto.Signer) (handshaker, error) { return newDummyHandshaker(s) }},
	{"tls", func(s crypto.Signer) (handshaker, error) { return newTLSHandshaker(s) }},
	{"ssh", func(s crypto.Signer) (handshaker, error) { return newSSHHandshaker(s) }},
}

type testFunc struct {
	name string
	f    func(t *testing.T)
}

func TestAllHandshakes(t *testing.T) {
	for _, htt := range hsTestTypes {
		for _, backwards := range []bool{false, true} {
			tp := &hsTestParams{}

			for _, test := range []testFunc{
				testFunc{
					"testEcho",
					tp.testEcho,
				},
				testFunc{
					"testGetsCorrectAuthenticatedPublicKey",
					tp.testGetsCorrectAuthenticatedPublicKey,
				},
				testFunc{
					"testCloseClientThenServerConns",
					tp.testCloseClientThenServerConns,
				},
				testFunc{
					"testCloseClientConnDuringServerRead",
					tp.testCloseClientConnDuringServerRead,
				},
				testFunc{
					"testWriteServerAfterClose",
					tp.testWriteServerAfterClose,
				},
				testFunc{
					"testClientWriteAndReadAllCloseServerThenClient",
					tp.testClientWriteAndReadAllCloseServerThenClient,
				},
			} {
				tp.clientKey = newSignerP521()
				tp.serverKey = newSignerP521()
				clientHS, _ := htt.newHS(tp.clientKey)
				serverHS, _ := htt.newHS(tp.serverKey)
				client, server := newTCPConns()
				clientConn, serverConn := newHSClientAndServer(client, server, clientHS, serverHS, tp.clientKey, tp.serverKey)
				defer clientConn.Close()
				defer serverConn.Close()
				tp.clientConn = clientConn
				tp.serverConn = serverConn

				dir := "standard"

				if backwards {
					// reverse client and server roles.
					tp.clientConn, tp.serverConn = tp.serverConn, tp.clientConn
					tp.clientKey, tp.serverKey = tp.serverKey, tp.clientKey
					dir = "reversed"
				}

				name := fmt.Sprintf("%s-%s-%s", test.name, htt.name, dir)
				if !t.Run(name, test.f) {
					break
				}
			}
		}
	}
}

type hsTestParams struct {
	clientConn, serverConn Conn
	clientKey, serverKey   crypto.Signer
}

func (ht *hsTestParams) testEcho(t *testing.T) {
	assert := assert.New(t)

	errs := make(chan error, 2)

	go func() {
		// read from client
		buf := make([]byte, 64)
		i, err := ht.serverConn.Read(buf)
		if err != nil {
			errs <- err
			return
		}
		// send the same message back
		_, err = ht.serverConn.Write(buf[0:i])
		if err != nil {
			errs <- err
			return
		}
	}()

	input := []byte("hello world\n")

	_, err := ht.clientConn.Write(input)
	if err != nil {
		t.Fatal(err)
	}

	response := make(chan []byte, 1)

	go func() {
		buf := make([]byte, 64)
		i, err := ht.clientConn.Read(buf)
		if err != nil {
			errs <- err
			return
		}
		response <- buf[0:i]
	}()

	select {
	case err := <-errs:
		t.Error(err)
	case r := <-response:
		assert.Equal(input, r)
	}
}

func (ht *hsTestParams) testGetsCorrectAuthenticatedPublicKey(t *testing.T) {
	assert := assert.New(t)
	expectedKey, _ := SerialisePublic(ht.clientKey.Public())
	assert.Equal(expectedKey, ht.serverConn.RemoteAddr().String())
}

// close without any reads or writes in either direction.
func (ht *hsTestParams) testCloseClientThenServerConns(t *testing.T) {
	assert := assert.New(t)

	assert.NoError(ht.clientConn.Close())
	assert.NoError(ht.serverConn.Close())
}

// close client (without writing) while server is reading. Then close server.
func (ht *hsTestParams) testCloseClientConnDuringServerRead(t *testing.T) {

	assert := assert.New(t)

	errs := make(chan error)
	go func() {
		buf := make([]byte, 1024)
		_, err := ht.serverConn.Read(buf)
		errs <- err
	}()

	assert.NoError(ht.clientConn.Close())
	assert.Equal(io.EOF, <-errs)
	assert.NoError(ht.serverConn.Close())

	// multiple closes should error
	assert.Equal(errHsConnectionClosed, ht.serverConn.Close())
	assert.Equal(errHsConnectionClosed, ht.clientConn.Close())
}

// after closing an end of the connection, write to that same end.
func (ht *hsTestParams) testWriteServerAfterClose(t *testing.T) {
	assert := assert.New(t)

	assert.NoError(ht.serverConn.Close())
	buf := make([]byte, 1024)

	_, err := ht.serverConn.Write(buf)
	assert.Equal(errHsConnectionClosed, err)

	assert.NoError(ht.clientConn.Close())
}

func (ht *hsTestParams) testClientWriteAndReadAllCloseServerThenClient(t *testing.T) {
	// TODO : rename this test to something like testEchoWithServerClose?
	assert := assert.New(t)

	msg := []byte("hello world")
	_, err := ht.clientConn.Write(msg)
	assert.NoError(err)

	buf := make([]byte, 1024)
	i, err := io.ReadAtLeast(ht.serverConn, buf, len(msg))
	assert.NoError(err)
	_, err = ht.serverConn.Write(buf[0:i])
	assert.NoError(err)
	assert.NoError(ht.serverConn.Close())

	bytes, err := ioutil.ReadAll(ht.clientConn)
	assert.NoError(err)
	_ = bytes
	assert.Equal(msg, bytes)

	assert.NoError(ht.clientConn.Close())
}
