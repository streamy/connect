package connect

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/ed25519"
	"io"
	"testing"
)

func TestKeys(t *testing.T) {
	assert := assert.New(t)

	type signerFunc func() (crypto.Signer, error)
	signerFuncs := []signerFunc{
		func() (crypto.Signer, error) { return ecdsa.GenerateKey(elliptic.P224(), rand.Reader) },
		func() (crypto.Signer, error) { return ecdsa.GenerateKey(elliptic.P256(), rand.Reader) },
		func() (crypto.Signer, error) { return ecdsa.GenerateKey(elliptic.P384(), rand.Reader) },
		func() (crypto.Signer, error) { return ecdsa.GenerateKey(elliptic.P521(), rand.Reader) },
		func() (crypto.Signer, error) { _, priv, err := ed25519.GenerateKey(rand.Reader); return priv, err },
	}

	for _, sf := range signerFuncs {
		signer, err := sf()
		if err != nil {
			t.Fatal(err)
		}
		ser, err := SerialiseSigner(signer)
		if err != nil {
			t.Error(err)
		}
		deser, err := DeserialiseSigner(ser)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(signer, deser)

		pub := signer.Public()
		pubSer, err := SerialisePublic(pub)
		if err != nil {
			t.Error(err)
		}
		pubDeser, err := DeserialisePublic(pubSer)
		if err != nil {
			t.Error(err)
		}
		assert.Equal(pub, pubDeser)

	}
}

func TestDeserialisePublicInvalid(t *testing.T) {
	for _, invalid := range []string{
		"",
		":",
		"x:x",

		"ecdsa:zz",
		"ecdsa:zz:zz",
		"ecdsa:p521:zz",
		"ecdsa:p521:YQ==",

		"ed25519:zz",
		"ed25519:YQ==",
	} {
		if _, err := DeserialisePublic(invalid); err == nil {
			t.Errorf("expected error Deserialising public key '%v'", invalid)
		}
	}
}

func TestDeserialiseSignerInvalid(t *testing.T) {
	for _, invalid := range []string{
		"",
		":",
		"x:x",

		"ecdsa:zz",
		"ecdsa:zz:zz",
		"ecdsa:p521:zz",
		//	"ecdsa:p521:YQ==", FIXME: re-enable this and make test pass

		"ed25519:zz",
		"ed25519:YQ==",
	} {
		if _, err := DeserialiseSigner(invalid); err == nil {
			t.Errorf("expected error Deserialising public key '%v'", invalid)
		}
	}
}

func TestSerialisePublicInvalid(t *testing.T) {
	for _, invalid := range []interface{}{nil, ""} {
		_, err := SerialisePublic(invalid)
		if err == nil {
			t.Errorf("expected error Serialising %+v", invalid)
		}
	}
}

func TestSerialiseSignerInvalid(t *testing.T) {
	for _, invalid := range []crypto.Signer{nil, dummySigner{}} {
		_, err := SerialiseSigner(invalid)
		if err == nil {
			t.Errorf("expected error Deserialising %+v", invalid)
		}
	}
}

type dummySigner struct{}

func (ds dummySigner) Public() crypto.PublicKey {
	panic("")
}

func (ds dummySigner) Sign(io.Reader, []byte, crypto.SignerOpts) ([]byte, error) {
	panic("")
}
