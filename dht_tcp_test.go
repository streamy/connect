package connect

import (
	"context"
	"net"
	"sync"
	"testing"
	"time"

	"github.com/nictuku/dht"
	"github.com/stretchr/testify/assert"
)

func TestTCPAnnounceResolve(t *testing.T) {
	assert := assert.New(t)

	publicKey := newSigner().Public()

	sn, err := dhtServiceName(publicKey, "tcp")
	assert.NoError(err)
	snBytes := []byte(sn)
	t.Logf("info hash is %x\n", snBytes)

	// start router dht node
	d := runDHTRouter(4444)
	defer d.Stop()

	time.Sleep(100 * time.Millisecond)

	hostAndPort := "127.0.0.1:2222"

	ta, err := net.ResolveTCPAddr("tcp", hostAndPort)
	assert.NoError(err)

	at := dhtTCPAnnounceType{routers: "localhost:4444"}
	ann, err := at.Announce(&Addr{publicKey}, ta)
	assert.NoError(err)

	defer ann.Close()

	time.Sleep(800 * time.Millisecond)

	resolver := newDHTTCPResolver("localhost:4444")

	candidates := make(chan net.Addr)

	ctx, cancel := context.WithCancel(context.Background())
	var wg sync.WaitGroup

	errs := make(chan error, 1)
	wg.Add(1)
	go func() {
		defer wg.Done()
		err := resolver.resolve(ctx, publicKey, candidates)
		if err != nil {
			errs <- err
		}
	}()

	select {
	case err := <-errs:
		t.Fatal(err)
	case candidate := <-candidates:
		assert.Equal(hostAndPort, candidate.String())
	}
	cancel()

	wg.Wait()
}

func runDHTRouter(port int) *dht.DHT {

	conf := dht.NewConfig()
	conf.DHTRouters = ""
	conf.SaveRoutingTable = false
	conf.ClientPerMinuteLimit = 1024 // TODO: understand why we have to set this so high.
	conf.Port = port

	d, err := dht.New(conf)
	if err != nil {
		panic(err)

	}

	if err = d.Start(); err != nil {
		panic(err)
	}

	return d
}
