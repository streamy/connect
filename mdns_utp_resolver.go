package connect

import (
	"context"
	"crypto"
	"log"
	"net"
	"sync"

	"github.com/micro/mdns"
)

func newMDNSUTPResolver() resolver {
	return &mdnsUTPResolver{}
}

type mdnsUTPResolver struct {
}

func (d *mdnsUTPResolver) resolve(ctx context.Context, pk crypto.PublicKey, resolved chan net.Addr) error {
	var wg sync.WaitGroup

	defer wg.Wait()

	sn, err := mdnsServiceName(pk, "utp")
	if err != nil {
		return err
	}

	results := make(chan *mdns.ServiceEntry)
	errs := make(chan error, 1)

	wg.Add(1)
	go func() {
		defer wg.Done()

		for {
			select {
			case <-ctx.Done():
				errs <- ctx.Err()
				return
			default:
				if err := mdns.Query(&mdns.QueryParam{
					Domain:              "",
					Entries:             results,
					Service:             sn,
					Context:             ctx,
					WantUnicastResponse: true,
				}); err != nil {
					errs <- err
				}
			}
		}
	}()

	wantedName := sn + "." + sn + ".local."

	for {
		select {
		case err := <-errs:
			log.Printf("TODO: mdnsUTPResolver: some errors should be retryable: %v\n", err)
			return err
		case se := <-results:
			if se.Name == wantedName {
				var ip net.IP = se.AddrV4
				if ip == nil {
					// v4 or v6 should both be fine, but be
					// prepared for ipv6 only networks
					ip = se.AddrV6
				}
				a := &net.UDPAddr{
					IP:   ip,
					Port: se.Port,
				}
				select {
				case resolved <- a:
				case <-ctx.Done():
					return ctx.Err()
				}
			}

		case <-ctx.Done():
			return ctx.Err()
		}
	}
}
