package connect

import (
	"io"
)

func closeAll(closer ...io.Closer) error {
	errCh := make(chan error)
	for _, c := range closer {
		go func(c io.Closer) {
			if c != nil {
				errCh <- c.Close()
			} else {
				errCh <- nil
			}
		}(c)
	}

	var errs []error
	for range closer {
		err := <-errCh
		if err != nil {
			errs = append(errs, err)
		}
	}
	switch len(errs) {
	case 0:
		return nil
	case 1:
		return errs[0]
	default:
		return multiError{"close failed", errs}
	}
}
